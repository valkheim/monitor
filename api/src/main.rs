#![feature(plugin)]
#![plugin(rocket_codegen)]

extern crate rocket;
extern crate serde_json;
extern crate systemstat;
extern crate shiplift;

#[macro_use] extern crate serde_derive;
#[macro_use] extern crate rocket_contrib;

mod infos;
mod system;
mod apps;

use rocket_contrib::{Json, Value};

#[get("/")]
fn index() -> Json<Value> {
    let infos = infos::get();

    Json(json!(infos))
}

fn main() {
    rocket::ignite()
        .mount("/", routes![index])
        .launch();
}
