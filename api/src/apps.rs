use shiplift::Docker;

#[derive(Serialize)]
pub struct Apps {
    pub foo: String,
}

pub fn get_apps_infos() -> Apps {
    let docker = Docker::new();
    let images = docker.images();
    for i in images.list(&Default::default()).unwrap() {
        println!("-> {:?}", i);
    }

    Apps {
        foo: "bar".to_string(),
    }
}
