use std::process::Command;
use std::process::Output;

extern crate systemstat;
use self::systemstat::{System, Platform};

#[derive(Serialize)]
pub struct SystemInfos {
    pub os: String,
    pub arch: String,
    pub hostname: String,
    pub uptime: systemstat::Duration,
}

fn get_stdout_from(output: Output) -> String {
    let mut out = String::from("Unk");
    if output.status.success() {
        out = String::from_utf8_lossy(&output.stdout).to_string();
        out.pop();
    }

    out
}

fn get_uptime(sys: System) -> systemstat::Duration {
    match sys.uptime() {
        Ok(uptime) => uptime,
        Err(x) => {
            println!("\nUptime: error: {}", x);

            systemstat::Duration::new(0, 0)
        }
    }
}

fn get_os() -> String {
    let output = Command::new("/bin/uname")
        .arg("-o")
        .output()
        .expect("failed to execute process");

    get_stdout_from(output)
}

fn get_arch() -> String {
    let output = Command::new("/bin/uname")
        .arg("-m")
        .output()
        .expect("failed to execute process");

    get_stdout_from(output)
}

fn get_hostname() -> String {
    let output = Command::new("/bin/hostname")
        .output()
        .expect("failed to execute process");

    get_stdout_from(output)
}

pub fn get_system_infos() -> SystemInfos {
    let sys = System::new();

    SystemInfos {
        os: get_os(),
        arch: get_arch(),
        hostname: get_hostname(),
        uptime: get_uptime(sys),
    }
}
