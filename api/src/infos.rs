// https://github.com/myfreeweb/systemstat

use system;
use apps;

#[derive(Serialize)]
pub struct Infos {
    system: system::SystemInfos,
    apps: apps::Apps,
}

pub fn get() -> Infos {
    Infos {
        system: system::get_system_infos(),
        apps: apps::get_apps_infos()
    }
}
