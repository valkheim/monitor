# Monitor

## What's Monitor ?

It's a nice tool that will help you to monitor your server with a nice restful API and a fancy frontal.

## Roadmap

* Build a restful API in rust with the help of [rocket.rs](https://rocket.rs)
* Build a frontend in ReactJS (based on a [CreativeTim](https://www.creative-tim.com/) [template](https://demos.creative-tim.com/light-bootstrap-dashboard-react/#/dashboard))
* Dockerize the application

Infos :
*  Disk space
*  Disk usage
*  RAM space
*  RAM usage
*  Logged users
*  Average load
*  Docker infos (link with [Portainer](https://portainer.io/))
*  Restart process
*  Auth
*  Inspect file(s)

*  add security features of LinEnumGui.sh
